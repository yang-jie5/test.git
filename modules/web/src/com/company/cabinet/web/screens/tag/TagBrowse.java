package com.company.cabinet.web.screens.tag;

import com.company.cabinet.service.HttpSpdService;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.components.Button;
import com.haulmont.cuba.gui.components.ButtonsPanel;
import com.haulmont.cuba.gui.components.DataGrid;
import com.haulmont.cuba.gui.components.ListComponent;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.screen.*;
import com.company.cabinet.entity.Tag;
import com.alibaba.fastjson.JSONObject;

import javax.inject.Inject;

@UiController("cabinet_Tag.browse")
@UiDescriptor("tag-browse.xml")
@LookupComponent("tagsTable")
@LoadDataBeforeShow
public class TagBrowse extends StandardLookup<Tag> {
    @Inject
    private CollectionContainer<Tag> tagsDc;
    @Inject
    private ButtonsPanel buttonsPanel;
    @Inject
    private DataGrid<Tag> tagsTable;
    @Inject
    private Button spd;
    @Inject
    private HttpSpdService httpSpdService;
    @Inject
    private Notifications notifications;
    @Inject
    private MessageBundle messageBundle;

    @Subscribe("spd")
    public void buttonClick(Button.ClickEvent event){
         String url="http://192.168.1.128:8080/travel/testServlet";
         String token="";
        String s = httpSpdService.httpRequest(url, token);
        JSONObject json =JSONObject.parseObject(s);
        String status = (String) json.get("status");
        System.out.println(status);
        if(status.equals("接收成功")){
           notifications.create().
                   withCaption(messageBundle.getMessage("接收成功"))
                   .withType(Notifications.NotificationType.HUMANIZED)
                   .show();
           System.out.println(s);
       }else {
           notifications.create().
                   withCaption(messageBundle.getMessage("接收失败"))
                   .withType(Notifications.NotificationType.ERROR)
                   .show();
       }


    }

}