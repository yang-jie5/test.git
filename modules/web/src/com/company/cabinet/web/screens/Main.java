package com.company.cabinet.web.screens;

import com.haulmont.cuba.gui.screen.Screen;
import com.haulmont.cuba.gui.screen.UiController;
import com.haulmont.cuba.gui.screen.UiDescriptor;

@UiController("cabinet_Main")
@UiDescriptor("main.xml")
public class Main extends Screen {
}