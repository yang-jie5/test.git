package com.company.cabinet.web.screens.tag;

import com.haulmont.cuba.gui.components.LookupField;
import com.haulmont.cuba.gui.screen.*;
import com.company.cabinet.entity.Tag;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@UiController("cabinet_Tag.edit")
@UiDescriptor("tag-edit.xml")
@EditedEntityContainer("tagDc")
@LoadDataBeforeShow
public class TagEdit extends StandardEditor<Tag> {

    @Inject
    private LookupField<String> tagStatusField;
    @Inject
    private LookupField<String> materialStatusField;

    @Subscribe
    public  void Init(InitEvent event){
        List<String> list=new ArrayList<>();
        list.add("未入柜");
        list.add("入柜");
        tagStatusField.setOptionsList(list);
        List<String> list1=new ArrayList<>();
        list1.add("未入库");
        list1.add("已入库");
        list1.add("已出库");
        list1.add("已领未还");
        materialStatusField.setOptionsList(list1);
    }
}