package com.company.cabinet.service;

import java.util.Date;
import java.util.List;

public interface StatisticNewService {
    String NAME = "medicalcabinet_StatisticNewService";

    public String getCount1(String date,int facility);
}