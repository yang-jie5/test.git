package com.company.cabinet.service;

import com.company.cabinet.entity.Tag;

public interface HttpSpdService {
    String NAME = "medicalcabinet_HttpSpdService";
    public String httpRequest(String url, String token);
    public String httpRequestTag(String url, String token, Tag tag);
    public String spdSendTag(String materialName,String materialCode);
    public String spdSendTag1(String materials);
}