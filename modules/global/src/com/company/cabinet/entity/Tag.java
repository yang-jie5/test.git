package com.company.cabinet.entity;

import com.haulmont.cuba.core.entity.BaseIntegerIdEntity;
import com.haulmont.cuba.core.entity.HasUuid;
import com.haulmont.cuba.core.entity.annotation.Listeners;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Table(name = "CABINET_TAG")
@Entity(name = "cabinet_Tag")
@Listeners("medicalcabinet_TagListener")
public class Tag extends BaseIntegerIdEntity {
    private static final long serialVersionUID = 8732485554443595660L;

    @Column(name ="facility_id")
    private  String facility;
    @Column(name = "materialCode",nullable = false)
    private  String materialCode;
    @Column(name = "materialName",nullable = false)
    private  String materialName;
    @Column(name = "rfidCode")
    private  String rfidCode;
    @Column(name = "tagStatus")
    private  String tagStatus;
    @Column(name = "orderCode")
    private  String orderCode;
    @Column(name = "materialType",nullable = false)
    private  String materialType;
    @Column(name = "materialStandard",nullable = false)
    private  String materialStandard;
    @Column(name = "producer",nullable = false)
    private  String producer;
    @Column(name = "provider",nullable = false)
    private  String provider;
    @Column(name = "produceTime",nullable = false)
    private Date produceTime;
    @Column(name = "inFacilityTime")
    private  Date inFacilityTime;
    @Column(name = "validTime",nullable = false)
    private  Date validTime;
    @Column(name = "materialStatus",nullable = false)
    private  String materialStatus;
    @Column(name = "units",nullable = false)
    private  String units;
    @Column(name = "letter")
    private  String letter;
    @Column(name = "outputType")
    private  String outputType;
    @Column(name = "outputReason")
    private  String outputReason;
    @Column(name = "areaSet")
    private  String areaSet;
    @Column(name = "remainingDays")
    private  Integer remainingDays;
    @Column(name = "temperature")
    private  String temperature;

    public Integer getRemainingDays() {
        return remainingDays;
    }

    public void setRemainingDays(Integer remainingDays) {
        this.remainingDays = remainingDays;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getFacility() {
        return facility;
    }

    public void setFacility(String facility) {
        this.facility = facility;
    }

    public String getMaterialCode() {
        return materialCode;
    }

    public void setMaterialCode(String materialCode) {
        this.materialCode = materialCode;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public String getRfidCode() {
        return rfidCode;
    }

    public void setRfidCode(String rfidCode) {
        this.rfidCode = rfidCode;
    }

    public String getTagStatus() {
        return tagStatus;
    }

    public void setTagStatus(String tagStatus) {
        this.tagStatus = tagStatus;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public String getMaterialType() {
        return materialType;
    }

    public void setMaterialType(String materialType) {
        this.materialType = materialType;
    }

    public String getMaterialStandard() {
        return materialStandard;
    }

    public void setMaterialStandard(String materialStandard) {
        this.materialStandard = materialStandard;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public Date getProduceTime() {
        return produceTime;
    }

    public void setProduceTime(Date produceTime) {
        this.produceTime = produceTime;
    }

    public Date getInFacilityTime() {
        return inFacilityTime;
    }

    public void setInFacilityTime(Date inFacilityTime) {
        this.inFacilityTime = inFacilityTime;
    }

    public Date getValidTime() {
        return validTime;
    }

    public void setValidTime(Date validTime) {
        this.validTime = validTime;
    }

    public String getMaterialStatus() {
        return materialStatus;
    }

    public void setMaterialStatus(String materialStatus) {
        this.materialStatus = materialStatus;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getLetter() {
        return letter;
    }

    public String getOutputType() {
        return outputType;
    }

    public void setOutputType(String outputType) {
        this.outputType = outputType;
    }

    public String getOutputReason() {
        return outputReason;
    }

    public void setOutputReason(String outputReason) {
        this.outputReason = outputReason;
    }

    public void setLetter(String letter) {
        this.letter = letter;
    }

    public String getAreaSet() {
        return areaSet;
    }

    public void setAreaSet(String areaSet) {
        this.areaSet = areaSet;
    }
}