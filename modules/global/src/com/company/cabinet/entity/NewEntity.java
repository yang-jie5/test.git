package com.company.cabinet.entity;

import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Table(name = "MEDICALCABINET_NEW_ENTITY")
@Entity(name = "medicalcabinet_NewEntity")
public class NewEntity extends StandardEntity {
    private static final long serialVersionUID = -2873287377904244861L;
}