-- begin CABINET_TAG
create table CABINET_TAG (
    ID integer,
    --
    facility_id varchar(255),
    materialCode varchar(255) not null,
    materialName varchar(255) not null,
    rfidCode varchar(255),
    tagStatus varchar(255),
    orderCode varchar(255),
    materialType varchar(255) not null,
    materialStandard varchar(255) not null,
    producer varchar(255) not null,
    provider varchar(255) not null,
    produceTime datetime(3) not null,
    inFacilityTime datetime(3),
    validTime datetime(3) not null,
    materialStatus varchar(255) not null,
    units varchar(255) not null,
    letter varchar(255),
    outputType varchar(255),
    outputReason varchar(255),
    areaSet varchar(255),
    remainingDays integer,
    temperature varchar(255),
    --
    primary key (ID)
)^
-- end CABINET_TAG
