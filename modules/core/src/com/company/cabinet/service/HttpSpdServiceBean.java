package com.company.cabinet.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.company.cabinet.entity.Tag;
import com.haulmont.cuba.core.global.DataManager;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.apache.http.protocol.HTTP;

import java.io.IOException;
import java.util.List;

import org.springframework.stereotype.Service;

import javax.inject.Inject;

@Service(HttpSpdService.NAME)
public class HttpSpdServiceBean implements HttpSpdService {
    @Inject
    private DataManager dataManager;
    /**
     * http向SPD系统请求服务
     * @param url  请求路径
     * @param token  token标志符号
     * @return 返回参数
     */
    @Override
    public String httpRequest(String url,String token) {
       return null;
    }

    @Override
    public String httpRequestTag(String url, String token, Tag tag) {

        return  null;
    }

    @Override
    public String spdSendTag(String materialName,String materialCode) {

        return  null;
    }

    @Override
    public String spdSendTag1(String materials) {//传输JSONArray
        return  null;
    }


    /**
     *http 发送post请求
     * @param url  请求的URL网址
     * @param encoding  编码格式
     *
     */
    public  static  String sendPost(String url,JSONArray jsonArray,String token, String encoding) throws  IOException{
        return  null;
    }
}