package com.company.cabinet.core;

import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.company.cabinet.entity.Tag;
import com.company.cabinet.service.InventoryService;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.security.app.Authenticated;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

@Component(Message.NAME)
public class Message {
    public static final String NAME = "medicalcabinet_Message";
    @Inject
    private InventoryService inventoryService;
    @Inject
    private DataManager dataManager;


}