package com.company.cabinet.core;

import com.company.cabinet.entity.Tag;
import com.company.cabinet.service.HttpSpdService;
import com.haulmont.cuba.core.listener.AfterDeleteEntityListener;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.sql.Connection;

@Component(TagListener.NAME)
public class TagListener implements AfterDeleteEntityListener<Tag> {
    public static final String NAME = "medicalcabinet_TagListener";
    @Inject
    private HttpSpdService httpSpdService;
    @Override
    public void onAfterDelete(Tag entity, Connection connection) {

    }
}